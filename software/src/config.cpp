#include "main.h"

const String cyclesSetsFilenames[MAX_CYCLES] = {
  "set001.csv",
  "set002.csv",
  "set003.csv",
  "set004.csv",
  "set005.csv",
  "set006.csv",
  "set007.csv",
  "set008.csv"
};


// TODO
// Define the default cycles here, which
// are then updated in the code by reading the file
Cycle cycles[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

Cycle set001[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

Cycle set002[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

Cycle set003[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

Cycle set004[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

Cycle set005[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

Cycle set006[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

Cycle set007[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};

Cycle set008[MAX_CYCLES] = {
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""},
  {0, 0.0, 0, 0, ""}
};


CyclesSet cyclesSet001 = { *set001 };
CyclesSet cyclesSet002 = { *set002 };
CyclesSet cyclesSet003 = { *set003 };
CyclesSet cyclesSet004 = { *set004 };
CyclesSet cyclesSet005 = { *set005 };
CyclesSet cyclesSet006 = { *set006 };
CyclesSet cyclesSet007 = { *set007 };
CyclesSet cyclesSet008 = { *set008 };
CyclesSet cyclesSets[MAX_CYCLES] = {
  cyclesSet001,
  cyclesSet002,
  cyclesSet003,
  cyclesSet004,
  cyclesSet005,
  cyclesSet006,
  cyclesSet007,
  cyclesSet008,
};

//float chaosIntensities[MAX_CYCLES] = {0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};

Env envelopeValues[MAX_CYCLES] = {
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000},
  {60, 30, 0.5, 1000, 1000}
};

int noteDurations[MAX_CYCLES] = {
  500,
  500,
  500,
  500,
  500,
  500,
  500,
  500
};

int noteDurationsCounter[MAX_CYCLES] = {
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0
};

int triggerDurations[MAX_CYCLES] = {
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0
};

//int triggerPins[MAX_CYCLES] = {41, 40, 39, 38, 37, 36, 35, 34};
int chaosPins[MAX_CHAOS_PINS] = {A10, A11, A8};
int triggerPins[MAX_CYCLES] = {39, 38, 37, 36, 35, 34, 33, 32};
// I am so silly...didn't arrange the pots correctly on the board...ooops
int chaosIntensityPins[MAX_CYCLES] = {A0, A4, A1, A5, A2, A16, A3, A17};
float chaosIntensities[MAX_CYCLES] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

float defaultNoteRatios[MAX_CYCLES] = {
  1.0,
  6.0/5.0,
  4.0/3.0,
  3.0/2.0,
  44.0/27.0,
  40.0/21.0,
  2.0/1.0,
  9.0/4.0
};

float noteRatios[MAX_CYCLES] = {
  1.0,
  6.0/5.0,
  4.0/3.0,
  3.0/2.0,
  44.0/27.0,
  40.0/21.0,
  2.0/1.0,
  9.0/4.0
};

float fundamentalFrequency = DEFAULT_FUNAMDENTAL_FREQUENCY;

//float voiceDivisorsSet[MAX_VOICES + 1] = {0.0001, 0.001, 0.01, 0.1, 1.0, 10, 100, 1000, 10000};
float defaultVoiceScalesSet[MAX_VOICES + 1] = {0.0625, 0.125, 0.25, 0.5, 1.0, 2, 8, 16, 32};
float voiceScalesSet[MAX_VOICES + 1] = {0.0625, 0.125, 0.25, 0.5, 1.0, 2, 8, 16, 32};
float allVoiceScales[MAX_VOICES][MAX_CYCLES];

float latValue = DEFAULT_LAT;
float lonValue = DEFAULT_LON;

bool trigGate = false;

ResponsiveAnalogRead chaosCV(CHAOS_CV_PIN, true);
ResponsiveAnalogRead scaleCV(SCALE_CV_PIN, true);
uint16_t chaosCVValue = 0;
uint16_t scaleCVValue = 0;

// TODO
// There must be a more general way of doing this?
ResponsiveAnalogRead chaosPinX(chaosPins[0], true);
ResponsiveAnalogRead chaosPinY(chaosPins[1], true);
ResponsiveAnalogRead chaosPinZ(chaosPins[2], true);
ResponsiveAnalogRead *chaosPinsReads[MAX_CHAOS_PINS] = {
  &chaosPinX,
  &chaosPinY,
  &chaosPinZ
};
Smoothed <float> *chaosPinsAvgs[MAX_CHAOS_PINS];
Smoothed <float> chaosPinXAvg; 
Smoothed <float> chaosPinYAvg;
Smoothed <float> chaosPinZAvg;

u_int16_t chaosPinsValues[MAX_CHAOS_PINS] = {0, 0, 0};
ResponsiveAnalogRead chaosIntensityPot1(chaosIntensityPins[0], true);
ResponsiveAnalogRead chaosIntensityPot2(chaosIntensityPins[1], true);
ResponsiveAnalogRead chaosIntensityPot3(chaosIntensityPins[2], true);
ResponsiveAnalogRead chaosIntensityPot4(chaosIntensityPins[3], true);
ResponsiveAnalogRead chaosIntensityPot5(chaosIntensityPins[4], true);
ResponsiveAnalogRead chaosIntensityPot6(chaosIntensityPins[5], true);
ResponsiveAnalogRead chaosIntensityPot7(chaosIntensityPins[6], true);
ResponsiveAnalogRead chaosIntensityPot8(chaosIntensityPins[7], true);
ResponsiveAnalogRead *chaosIntensitiesPots[MAX_CYCLES] = {
  &chaosIntensityPot1,
  &chaosIntensityPot2,
  &chaosIntensityPot3,
  &chaosIntensityPot4,
  &chaosIntensityPot5,
  &chaosIntensityPot6,
  &chaosIntensityPot7,
  &chaosIntensityPot8
};


//LedControl display = LedControl(12, 11, 10, 1);
//LedControl(DIN_PIN, CLK_PIN, CS_PIN, 1);

const uint64_t IMAGES[] = {
  0xff000001010000ff, 0xff000003030000ff, 0xff000006060000ff,
  0xff00000c0c0000ff, 0xff000018180000ff, 0xff000030300000ff,
  0xff000060600000ff, 0xff0000c0c00000ff, 0xff000080800000ff,
  0xff0000c0c00000ff, 0xff000060600000ff, 0xff000018180000ff,
  0xff00000c0c0000ff, 0xff000006060000ff, 0xff000003030000ff,
  0xff000001010000ff
};
const int IMAGES_LEN = sizeof(IMAGES)/8;

const uint64_t bootImages[MAX_DISPLAY_ROWS + 1] = {
  0x0000000000000000,
  0x00000000000000ff,
  0x000000000000ffff,
  0x0000000000ffffff,
  0x00000000ffffffff,
  0x000000ffffffffff,
  0x0000ffffffffffff,
  0x00ffffffffffffff,
  0xffffffffffffffff
};

/*
  0x0101010101010101,
  0x0303030303030303,
  0x0707070707070707,
  0x0f0f0f0f0f0f0f0f,
  0x1f1f1f1f1f1f1f1f,
  0x3f3f3f3f3f3f3f3f,
  0x7f7f7f7f7f7f7f7f,
  0xffffffffffffffff

*/


LedControl_HW_SPI display = LedControl_HW_SPI();
//MD_Parola display = MD_Parola(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);
const char barValues[MAX_DISPLAY_ROWS + 1] = {0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff};
//const char barValues[MAX_DISPLAY_ROWS + 1] = {0x00, 0x08, 0x0c, 0x0e, 0x0f, 0xf8, 0xfc, 0xfe, 0xff};
/*
const uint64_t sigil_image000 = 0x83c3a498b8f4e2ff;
const uint64_t sigil_image001 = 0x99a5bd6666bda599;
const uint64_t sigil_image002 = 0xe496a96f0a0a0a0c;
const uint64_t sigil_image003 = 0x08783b1b1d81e3e3;
const uint64_t sigil_image004 = 0x1824dbdb24180000;
const uint64_t sigil_image005 = 0x22a2aaaaaaaaa222;
const uint64_t sigil_image006 = 0x42e54915224a82fe;
const uint64_t sigil_image007 = 0xe44a514e4e4e7e00;
*/
const uint64_t sigil_image000 = 0xff472f1d1925c3c1;
const uint64_t sigil_image001 = 0x99a5bd6666bda599;
const uint64_t sigil_image002 = 0x30505050f6956927;
const uint64_t sigil_image003 = 0xc7c781b8d8dc1e10;
const uint64_t sigil_image004 = 0x00001824dbdb2418;
const uint64_t sigil_image005 = 0x4445555555554544;
const uint64_t sigil_image006 = 0x7f415244a892a742;
const uint64_t sigil_image007 = 0x007e7272728a5227;

const uint64_t sigilList[MAX_CYCLES] = {
  sigil_image000, 
  sigil_image001, 
  sigil_image002, 
  sigil_image003, 
  sigil_image004, 
  sigil_image005, 
  sigil_image006, 
  sigil_image007 
  };

uint64_t scaleImage = 0; 

int midiNotes[MAX_CYCLES] = {
  TRIG1_MIDI,
  TRIG2_MIDI,
  TRIG3_MIDI,
  TRIG4_MIDI,
  TRIG5_MIDI,
  TRIG6_MIDI,
  TRIG7_MIDI,
  TRIG8_MIDI
};

int8_t midiFlags[MAX_CYCLES] = {-1, -1, -1, -1, -1, -1, -1, -1};


Bounce setButton(SET_BUTTON, 50);
uint8_t setIndex = 0;

bool trigGateState = true;

//using Transport = MIDI_NAMESPACE::SerialMIDI<HardwareSerial>;
//HardwareSerial mySerial = HardwareSerial(Serial2);
//Transport serialMIDI(mySerial);
//MyMIDI TestMIDI((Transport&)serialMIDI);

// Taken from here: <https://stackoverflow.com/questions/4548004/how-to-correctly-and-standardly-compare-floats>
bool areEqualRel(float a, float b, float epsilon)  {
  return (fabs(a - b) <= epsilon * max(fabs(a), fabs(b)));
}

void initializeChaosPinsAvgs() {
  XENOKINETICS_DEBUG_PRINTLN("Initializing moving average for chaos pins....");
  chaosPinXAvg.begin(SMOOTHED_AVERAGE, 10); 
  chaosPinYAvg.begin(SMOOTHED_AVERAGE, 10);
  chaosPinZAvg.begin(SMOOTHED_AVERAGE, 10);

  chaosPinsAvgs[0] = &chaosPinXAvg;
  chaosPinsAvgs[1] = &chaosPinYAvg;
  chaosPinsAvgs[2] = &chaosPinZAvg;

}

void initializeAllVoiceScales() {
  for (int i = 0; i < MAX_VOICES; i++) {
    for (int j = 0; j < MAX_CYCLES; j++) {
      allVoiceScales[i][j] = 1.0;
    }
  }
}