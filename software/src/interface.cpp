#include "main.h"

uint8_t currentInterfaceState = STATE_SIGIL;

long encoderKnobValue = -999;
Encoder encoderKnob(ENCODER_1, ENCODER_2);
Bounce encoderButton(ENCODER_BUTTON, 50);
Bounce divisorButton(DIVISOR_BUTTON, 50);
uint8_t currentVoice = 0;
bool stateDivisorsFirstProcess = true;

int64_t encoderKnobValues[MAX_CYCLES] = {40, 40, 40, 40, 40, 40, 40, 40};
float voiceScales[MAX_CYCLES] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
float currentVoiceScales[MAX_CYCLES] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
int blinkPeriod= 1000;
int blinkPeriodCounter[MAX_CYCLES] = {0, 0, 0, 0, 0, 0, 0, 0};
bool blinkDirection[MAX_CYCLES] = {false, false, false, false, false, false, false, false};

/**
 * Display an image based on a 64bit value.
 * Taken from <https://xantorohara.github.io/led-matrix-editor/>
 * 
*/
void displayImage(uint64_t image) {
  for (int i = 0; i < 8; i++) {
    byte row = (image >> i * 8) & 0xFF;
    for (int j = 0; j < 8; j++) {
      display.setLed(0, i, j, bitRead(row, j));
    }
  }
}

/**
 * Processes the state based on the value of the encoder knob
 * and button.
*/
void processInterfaceState(uint8_t state) {
    uint8_t voiceSetIndex = 4;
    float tempDivisor;
    if (state == STATE_SIGIL) {
        //XENOKINETICS_DEBUG_PRINTLN("In SIGIL state");
    } else if (state == STATE_DIVISORS) {
        //XENOKINETICS_DEBUG_PRINT("In DIVISORS state on voice ");
        //XENOKINETICS_DEBUG_PRINTLN(currentVoice);
        encoderKnobValues[currentVoice] = encoderKnobValue;

        voiceSetIndex = floor((int)map(encoderKnobValue, KNOB_MIN, KNOB_MAX, 0, 8));
        tempDivisor = voiceScalesSet[voiceSetIndex];
        /*
        if (tempDivisor != voiceScales[currentVoice]) {
            voiceScales[currentVoice] = tempDivisor;
            XENOKINETICS_DEBUG_PRINT("Changed divisor to: ");
            XENOKINETICS_DEBUG_PRINTLN(tempDivisor);
            updatePeriod(currentVoice, tempDivisor);
        }
        */
        if (tempDivisor != allVoiceScales[setIndex][currentVoice]) {
            allVoiceScales[setIndex][currentVoice] = tempDivisor;
            XENOKINETICS_DEBUG_PRINT("Changed divisor to: ");
            XENOKINETICS_DEBUG_PRINTLN(tempDivisor);
            updatePeriod(currentVoice, tempDivisor);
        }

        //XENOKINETICS_DEBUG_PRINT("Current divisor is set to ");
        //XENOKINETICS_DEBUG_PRINTLN(voiceScales[currentVoice]);
    }
}

/**
 * Process the incoming data from the chaos generator. We have an array of
 * ResponsiveAnalogRead's that we check and then save the values to a separate
 * Smoothed moving average array.
*/
void processChaosPins() {
    for (int i = 0; i < MAX_CHAOS_PINS; i++) {
        chaosPinsReads[i]->update();
        if (chaosPinsReads[i]->hasChanged()) {
            chaosPinsAvgs[i]->add(chaosPinsReads[i]->getValue());
            //chaosPinsValues[i] = chaosPinsReads[i]->getValue();
        }
    }

    //XENOKINETICS_DEBUG_PRINTLN(abs(chaosPinsAvgs[1]->get() - chaosPinsAvgs[0]->get()));
    /*
    XENOKINETICS_DEBUG_PRINT("X: ");
    XENOKINETICS_DEBUG_PRINTLN(chaosPinsAvgs[0]->get());
    XENOKINETICS_DEBUG_PRINT("Y: ");
    XENOKINETICS_DEBUG_PRINTLN(chaosPinsAvgs[1]->get());
    XENOKINETICS_DEBUG_PRINT("Z: ");
    XENOKINETICS_DEBUG_PRINTLN(chaosPinsAvgs[2]->get());
    */

}

/**
 * Process the pots for the chaos intensities.
 * TODO still need to finish writing this one.
*/
void processChaosIntensitiesPots() {
    for (int i = 0; i < MAX_CYCLES; i++) {
        chaosIntensitiesPots[i]->update();
        if (chaosIntensitiesPots[i]->hasChanged()) {
            XENOKINETICS_DEBUG_PRINT("pot changed: ");
            XENOKINETICS_DEBUG_PRINTLN(i + 1);
            chaosIntensities[i] = (float)map((float)chaosIntensitiesPots[i]->getValue(), 0.0, 1023.0, 1.0, 0.0);
            if (chaosIntensities[i] < 0.015) {
                chaosIntensities[i] = 0;
            };

            updateIntervalChaos(i);
            //tasks[i]->setInterval(1000);
        }
    }
}

/**
 * Process and update the display based on which state we're in,
 * SIGIL or DIVISORS.
*/
void processDisplay() {
    int index = 0;
    scaleImage = 0;
    for (int i = 0; i < MAX_DISPLAY_ROWS; i++) {
        // TODO
        // Need to figure out the right way of doing this...but this is basic for now
        // This may not be the most accurate way of doing things and should be updated
        index = floorf((float(encoderKnobValues[i]) / float(KNOB_MAX)) * 10);
        index = int(index - 1);
        if (index < 0) {index = 0;}
        if (index > (MAX_DISPLAY_ROWS)) {index = MAX_DISPLAY_ROWS;}
        // This is the way we try and blink the current voice that we're editing
        if (i == currentVoice) {
            if (blinkDirection[i] == false) {
                blinkPeriodCounter[i] += 1;
                scaleImage |= ((uint64_t)barValues[index] << (i*8));
            } else {
                blinkPeriodCounter[i] -= 1;
                scaleImage |= ((uint64_t)0 << (i*8));
            }

            if (blinkPeriodCounter[i] == blinkPeriod) {
                blinkDirection[i] = true;
            } else if (blinkPeriodCounter[i] == 0) {
                blinkDirection[i] = false;
            }

        } else {
            scaleImage |= ((uint64_t)barValues[index] << (i*8));
        }
    }

  if (currentInterfaceState == STATE_SIGIL) {
    displayImage(sigilList[setIndex]);
  } else if (currentInterfaceState == STATE_DIVISORS) {
    displayImage(scaleImage);
  }

}

/**
 * Process our set change button, switch sets,
 * update period based on current user interface state.
 * 
*/
void processButtons() {
  setButton.update();

  // If we have the falling edge...
  // update index or set back to 0
  if (setButton.fallingEdge() && (currentInterfaceState == STATE_SIGIL)) {
    setIndex += 1;
    if (setIndex >= MAX_CYCLES) {setIndex= 0;}

    float voiceScaleValue = 0;
    float minVoiceScaleValue = voiceScalesSet[0];
    float maxVoiceScaleValue = voiceScalesSet[MAX_VOICES];
    int encoderValue = 40;
    for (int i = 0; i < MAX_CYCLES; i++) {
        // cycles[MAX_CYCLES] is our global variable holding our current
        // set of cycles, so we update it here with the set that we
        // are on now.
        // We also update the period using the current voiceScales
        // that have been previously saved based on the interface state.
        cycles[i].period = cyclesSets[setIndex].cyclesSet[i].period;
        cycles[i].periodCorrected = cyclesSets[setIndex].cyclesSet[i].periodCorrected;
        cycles[i].offset = cyclesSets[setIndex].cyclesSet[i].offset;
        cycles[i].scale = cyclesSets[setIndex].cyclesSet[i].scale;
        strncpy(cycles[i].comment, cyclesSets[setIndex].cyclesSet[i].comment, MAX_COMMENT_LEN);
        //updatePeriod(i, voiceScales[i]);
        updatePeriod(i, allVoiceScales[setIndex][i]);
        for (int j = 0; j < (MAX_VOICES + 1); j++) {
            if (areEqualRel(voiceScalesSet[j], allVoiceScales[setIndex][i])) {
                voiceScaleValue = voiceScalesSet[j];
                encoderValue = (int) ceil(map(j, 0.0, (float) MAX_VOICES, (float) KNOB_MIN, (float) KNOB_MAX));
                //XENOKINETICS_DEBUG_PRINT("ENCODER VALUE IS: ");
                //XENOKINETICS_DEBUG_PRINTLN(encoderValue);
                encoderKnobValues[i] = encoderValue;
            }
        }
    }
    //XENOKINETICS_DEBUG_PRINTF("Current voice scales for set %d: \n", setIndex);
    //XENOKINETICS_DEBUG_PRINTF("%f %f %f %f %f %f %f %f\n", allVoiceScales[setIndex][0], allVoiceScales[setIndex][1], allVoiceScales[setIndex][2], allVoiceScales[setIndex][3], allVoiceScales[setIndex][4], allVoiceScales[setIndex][5], allVoiceScales[setIndex][6], allVoiceScales[setIndex][7]);
  };


}

/**
 * Simple method to set our global variable for the trig/gate state based on the switch.
*/
void processTrigGateSwitch() {
    trigGateState = digitalRead(TRIG_GATE_SW);
}

/**
 * The main method for checking all of our interface things...display,
 * pins, pots, buttons.
 * 
*/
void checkInterface() {
    processChaosPins();
    processChaosIntensitiesPots();

    if (chaosCV.hasChanged()) {
        XENOKINETICS_DEBUG_PRINT("Chaos CV raw value now: ");
        XENOKINETICS_DEBUG_PRINTLN(chaosCV.getValue());
    }

    encoderButton.update();
    // If we have the falling edge...
    // update state machine
    if (encoderButton.fallingEdge()) {
        if (currentInterfaceState == STATE_SIGIL) {
            XENOKINETICS_DEBUG_PRINTLN("switching to state divisors");
            currentInterfaceState = STATE_DIVISORS;
            encoderKnob.write(encoderKnobValues[currentVoice]);
        } else if ((currentInterfaceState == STATE_DIVISORS) && (currentVoice == (MAX_CYCLES - 1))) {
            XENOKINETICS_DEBUG_PRINTLN("switching to state sigil");
            currentInterfaceState = STATE_SIGIL;
            currentVoice = 0;
            encoderKnob.write(encoderKnobValues[currentVoice]);
        } else if ((currentInterfaceState == STATE_DIVISORS) && (currentVoice != (MAX_CYCLES - 1))) {
            XENOKINETICS_DEBUG_PRINTLN("continuing in state divisors");
            currentVoice += 1;
            encoderKnob.write(encoderKnobValues[currentVoice]);

        }
    }

    long newInputKnobValue = 0;

    // Read the input knob
    newInputKnobValue = encoderKnob.read();

    if (newInputKnobValue != encoderKnobValue) {
        encoderKnobValue = newInputKnobValue;
    }

    if (encoderKnobValue <= KNOB_MIN) {
        encoderKnobValue = KNOB_MIN;
    } else if (encoderKnobValue >= KNOB_MAX) {
        encoderKnobValue = KNOB_MAX;
    }

    processInterfaceState(currentInterfaceState);
    processDisplay();
    processButtons();
    processTrigGateSwitch();
}