#include "main.h"  

/**
 * Read through the voice scales file. Ensure that it is in the proper format!
 */  
void readVoiceScales(String filename) {
  File voiceScalesFile = SD.open(filename.c_str(), FILE_READ);
  uint8_t currentVoiceScaleItem = 0;
  float currentVoiceScale = 0.0;
  String currentLine;

  if (voiceScalesFile) {
    char* pEnd;
    while (voiceScalesFile.available()) {
      currentLine = voiceScalesFile.readStringUntil('\n');
      currentVoiceScale = strtof(currentLine.c_str(), &pEnd);

      if (XENOKINETICS_MEGA_DEBUG) {
          XENOKINETICS_DEBUG_PRINT("Current voice scale: ");
          XENOKINETICS_DEBUG_PRINTLN(currentVoiceScale);
      }

      voiceScalesSet[currentVoiceScaleItem] = currentVoiceScale;
      currentVoiceScaleItem += 1;

    }
    voiceScalesFile.close();
  } else {
    XENOKINETICS_DEBUG_PRINTLN("Error reading voice scales file! Using default values.");
    for (int i = 0; i < (MAX_VOICES + 1); i++) {
      voiceScalesSet[i] = defaultVoiceScalesSet[i];
    }
  }
}

/**
 * Read the frequencies file according to the given format.
 * Hopefully this is robust to some kinds of problems, but not all of them.
 * So be sure to have the properly formatted file!
*/
void readFrequencies(String filename) {
  File frequencesFile = SD.open(filename.c_str(), FILE_READ);
  uint8_t currentLineIndex = 0;
  String currentLine = "";
  String valueType = "ratio";
  float currentFrequency = 0.0;
  float currentValue = 0.0;
  float numerator = 0.0;
  float denominator = 1.0;
  float frequencyRatios[MAX_VOICES] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};


  if (frequencesFile) {
    char* pEnd;
    while (frequencesFile.available()) {
      if (currentLineIndex == 0) {
        currentLine = frequencesFile.readStringUntil('\n');
        currentFrequency = strtof(currentLine.c_str(), &pEnd);
        currentLineIndex += 1;
      } else if (currentLineIndex == 1) {
        currentLine = frequencesFile.readStringUntil('\n');

        if (currentLine.startsWith("decimal")) {
          valueType = "decimal";
        } else if (currentLine.startsWith("ratio")) {
          valueType = "ratio";
        } else {
          // TODO
          // is this a good default?
          valueType = "decimal";
        }

        currentLineIndex += 1;
      } else if (currentLineIndex > 1) {
        // TODO check on the decimal side of things
        if (valueType.equals("decimal")) {
          currentLine = frequencesFile.readStringUntil('\n');
          currentValue = strtof(currentLine.c_str(), &pEnd);
          frequencyRatios[currentLineIndex - 2] = currentValue;
        } else if (valueType.equals("ratio")) {
          currentLine = frequencesFile.readStringUntil('/');
          numerator = strtof(currentLine.c_str(), &pEnd);
          currentLine = frequencesFile.readStringUntil('\n');
          denominator = strtof(currentLine.c_str(), &pEnd);
          frequencyRatios[currentLineIndex - 2] = numerator/denominator;
        }

        // TODO
        // Actually implement the usage of these frequencies, like above
        noteRatios[currentLineIndex - 2] = frequencyRatios[currentLineIndex - 2];
        XENOKINETICS_DEBUG_PRINT("Current ratio: ");
        XENOKINETICS_DEBUG_PRINTLN(frequencyRatios[currentLineIndex - 2]);
        currentLineIndex += 1;
      }
    }
  } else {
    // TODO setup default values
    XENOKINETICS_DEBUG_PRINTLN("Error reading frequences file! Using default values.");
    fundamentalFrequency = DEFAULT_FUNAMDENTAL_FREQUENCY;

    for (int i = 0; i < MAX_CYCLES; i++) {
      noteRatios[i] = defaultNoteRatios[i];
    }

  }

}

/**
 * Read a particular cycle set and save it to our global set of cycles
 * 
 * NOTE: There is no data checking on the cycleIndex, so don't
 * overflow, please!
*/
void readCycleSet(String filename, uint8_t cycleIndex) {
  String period;
  String scale;
  String offset;
  String comment;
  uint64_t periodVal = 0;
  float scaleVal = 0;
  uint32_t offsetVal = 0;
  
  File cycleSetFile = SD.open(filename.c_str(), FILE_READ);

  int currentCycle = 0;
  if (cycleSetFile) {
    while (cycleSetFile.available()) {
      period = cycleSetFile.readStringUntil(',');
      char* pEnd;
      periodVal = strtoll(period.c_str(), &pEnd, 0);

      scale = cycleSetFile.readStringUntil(',');
      scaleVal = scale.toFloat();

      offset = cycleSetFile.readStringUntil(',');
      offsetVal = strtol(offset.c_str(), &pEnd, 0);

      comment = cycleSetFile.readStringUntil('\n');

      cyclesSets[cycleIndex].cyclesSet[currentCycle].period = periodVal;
      cyclesSets[cycleIndex].cyclesSet[currentCycle].scale = scaleVal;
      // TODO not sure if this is a good idea or not :)
      cyclesSets[cycleIndex].cyclesSet[currentCycle].periodCorrected = (uint64_t)((float)(periodVal - offsetVal) * scaleVal);
      cyclesSets[cycleIndex].cyclesSet[currentCycle].offset = offsetVal;
      //cycleSet[currentCycle].comment = comment;
      strncpy(cyclesSets[cycleIndex].cyclesSet[currentCycle].comment, comment.c_str(), MAX_COMMENT_LEN);

      if (XENOKINETICS_MEGA_DEBUG) {
          XENOKINETICS_DEBUG_PRINT("Cycle: ");
          XENOKINETICS_DEBUG_PRINTLN(currentCycle);
          XENOKINETICS_DEBUG_PRINT("Period (before division): ");
          XENOKINETICS_DEBUG_PRINTLN(cyclesSets[cycleIndex].cyclesSet[currentCycle].period);
          XENOKINETICS_DEBUG_PRINT("Scale: ");
          XENOKINETICS_DEBUG_PRINTLN(cyclesSets[cycleIndex].cyclesSet[currentCycle].scale);
          XENOKINETICS_DEBUG_PRINT("Period (after scaling and offset): ");
          XENOKINETICS_DEBUG_PRINTLN(cyclesSets[cycleIndex].cyclesSet[currentCycle].periodCorrected);
          XENOKINETICS_DEBUG_PRINT("Comment: ");
          XENOKINETICS_DEBUG_PRINTLN(cyclesSets[cycleIndex].cyclesSet[currentCycle].comment);
      }
      currentCycle += 1;
    }

    cycleSetFile.close();

    //cyclesSets[1]->cyclesSet = testSet;
    //testCyclesSet.cycle001.period; 
    //testCyclesSet.cyclesSet[0].period;
  } else {
    // TODO setup default values here, stored in config.h and used if for some reason
    // we can't read the SD card
    XENOKINETICS_DEBUG_PRINT("Error reading ");
    XENOKINETICS_DEBUG_PRINT(filename);
    XENOKINETICS_DEBUG_PRINTLN("!");
  }


}

/**
 * Read through all of our cycles sets from the given filenames,
 * and set the default to the first one. Save it in our global
 * cycles array.
*/
void readCyclesSets(const String (&filenames)[MAX_CYCLES]) {
  // Read through our 8 files and store them in our global
  // cyclesSet
  for (int i = 0; i < MAX_CYCLES; i++) {
    XENOKINETICS_DEBUG_PRINT("Reading cycle set file: ");
    XENOKINETICS_DEBUG_PRINTLN(filenames[i]);
    readCycleSet(filenames[i], i);
  }

  // Set our initial cycle set to set 0
  for (int i = 0; i < MAX_CYCLES; i++) {
    cycles[i].period = cyclesSets[0].cyclesSet[i].period;
    cycles[i].periodCorrected = cyclesSets[0].cyclesSet[i].periodCorrected;
    cycles[i].offset = cyclesSets[0].cyclesSet[i].offset;
    cycles[i].scale = cyclesSets[0].cyclesSet[i].scale;
    strncpy(cycles[i].comment, cyclesSets[0].cyclesSet[i].comment, MAX_COMMENT_LEN);


  }
}

