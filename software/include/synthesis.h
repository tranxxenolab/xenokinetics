#ifndef _XENOKINETICS_SYNTHESIS_H
#define _XENOKINETICS_SYNTHESIS_H

// GUItool: begin automatically generated code
extern AudioSynthWaveformModulated waveformMod7;   //xy=181.1999969482422,422.20001220703125
extern AudioSynthWaveformModulated waveformMod6;   //xy=183.1999969482422,370.1999969482422
extern AudioSynthWaveformModulated waveformMod5;   //xy=187.1999969482422,316.1999969482422
extern AudioSynthWaveformModulated waveformMod8;   //xy=197.1999969482422,475.1999969482422
extern AudioSynthWaveformModulated waveformMod3;   //xy=204.1999969482422,200.1999969482422
extern AudioSynthWaveformModulated waveformMod4;   //xy=204.1999969482422,258.1999969482422
extern AudioSynthWaveformModulated waveformMod1;   //xy=215.1999969482422,89.19999694824219
extern AudioSynthWaveformModulated waveformMod2;   //xy=215.1999969482422,148.1999969482422
extern AudioSynthWaveformModulated waveformMod9;   //xy=247.1999969482422,838.1999969482422
extern AudioEffectEnvelope      envelope6;      //xy=360.1999969482422,373.1999969482422
extern AudioEffectEnvelope      envelope8;      //xy=367.1999969482422,488.1999969482422
extern AudioEffectEnvelope      envelope5;      //xy=381.1999969482422,324.1999969482422
extern AudioEffectEnvelope      envelope2;      //xy=383.1999969482422,140.1999969482422
extern AudioEffectEnvelope      envelope7;      //xy=394.1999969482422,429.1999969482422
extern AudioEffectEnvelope      envelope1;      //xy=403.1999969482422,89.19999694824219
extern AudioEffectEnvelope      envelope3;      //xy=403.1999969482422,189.1999969482422
extern AudioEffectEnvelope      envelope4;      //xy=421.1999969482422,251.1999969482422
extern AudioMixer4              mixer1;         //xy=634.2000122070312,219.1999969482422
extern AudioMixer4              mixer2;         //xy=645.2000122070312,354.20001220703125
extern AudioMixer4              mixer3;         //xy=819.2000122070312,267.20001220703125
extern AudioOutputPT8211        pt8211_1;       //xy=969.2000122070312,265.20001220703125
// GUItool: end automatically generated code

extern AudioSynthWaveformModulated *waveforms[MAX_CYCLES];
extern AudioEffectEnvelope *envelopes[MAX_CYCLES];


/* this is for mqs
// GUItool: begin automatically generated code
extern AudioSynthWaveformModulated waveformMod2;   //xy=212.1999969482422,253.20001220703125
extern AudioSynthWaveformModulated waveformMod1;   //xy=213.1999969482422,211.1999969482422
extern AudioSynthWaveformModulated waveformMod3;   //xy=213.1999969482422,295.1999969482422
extern AudioSynthWaveformModulated waveformMod4;   //xy=214.1999969482422,338.20001220703125
extern AudioMixer4              mixer1;         //xy=445.1999969482422,271.1999969482422
extern AudioEffectEnvelope      envelope1;      //xy=643.2000122070312,219.1999969482422
extern AudioOutputMQS           mqs1;           //xy=820.2000122070312,224.1999969482422
// GUItool: end automatically generated code
*/


/* this is for using the audio shield
extern AudioSynthWaveformModulated waveformMod4;   //xy=203.1999969482422,343.1999969482422
extern AudioSynthWaveformModulated waveformMod3;   //xy=213.1999969482422,295.1999969482422
extern AudioSynthWaveformModulated waveformMod1;   //xy=219.1999969482422,208.1999969482422
extern AudioSynthWaveformModulated waveformMod2;   //xy=220.1999969482422,251.1999969482422
extern AudioMixer4              mixer1;         //xy=445.1999969482422,271.1999969482422
extern AudioEffectEnvelope      envelope1;      //xy=643.2000122070312,219.1999969482422
extern AudioOutputI2S           i2s1;           //xy=804.1999969482422,216.1999969482422
extern AudioControlSGTL5000     sgtl5000_1;     //xy=401.1999969482422,103.19999694824219
*/

#endif