#ifndef _XENOKINETICS_FILE_HANDLING_H
#define _XENOKINETICS_FILE_HANDLING_H

void readVoiceScales(const String filename);
void readFrequencies(const String filename);
//void readCycleSet(const String filename, Cycle (&cycleSet)[MAX_CYCLES]);
void readCycleSet(const String filename, uint8_t cycleIndex);
void readCyclesSets(const String (&filenames)[MAX_CYCLES]);

#endif
