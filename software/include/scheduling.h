#ifndef _XENOKINETICS_SCHEDULING_H
#define _XENOKINETICS_SCHEDULING_H


void t1Callback();
void t2Callback();
void t3Callback();
void t4Callback();
void t5Callback();
void t6Callback();
void t7Callback();
void t8Callback();

void turnVoiceOn(int voice);
void checkVoice(int voice);
void checkTrigger(int voice);

extern Scheduler runner; 

//Tasks
extern Task t1;
extern Task t2;
extern Task t3;
extern Task t4;
extern Task t5;
extern Task t6;
extern Task t7;
extern Task t8;


extern Task *tasks[MAX_CYCLES];

void updatePeriod(int currentCycle, float scale);
void updateIntervalChaos(int index);
void updateAllCycles();
void modifyRelease(uint8_t index);

#endif