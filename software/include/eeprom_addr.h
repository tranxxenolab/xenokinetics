#ifndef _XENOKINETICS_EEPROM_H
#define _XENOKINETICS_EEPROM_H

/*
Teensy 3.5 Output
Show Integer and Float Data Sizes

long long is 8
long is .... 4
int is ..... 4 <-- I expected 2, why 4?
short is ... 2
char is .... 1
double is .. 8
float is ... 4

from: <https://forum.pjrc.com/threads/54472-Is-size-of-int-type-correct>
*/

// Addresses for our variables in EEPROM
#define FIRST_RUN_ADDR                      0x00
#define EEPROM_LAT_ADDR                     FIRST_RUN_ADDR + sizeof(uint16_t)
#define EEPROM_LON_ADDR                     EEPROM_LAT_ADDR + sizeof(float)

#endif
