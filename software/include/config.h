#ifndef _XENOKINETICS_CONFIG_H
#define _XENOKINETICS_CONFIG_H

/*

Pin assignments:

* 41-34: Trigers 1-8 out
* 7: DIN
* 21: BCK
* 20: WS
* MIDI Serial:
  * 7: RX2
  * 8: TX2
* Rotary encoder: 3 pins, all digital
  * 2
  * 3
  * 4
* Display:
  * 10: CS
  * 11: DIN (MOSI?)
  * 13: SCK
* HC4067 multiplexer:
  * 8 channels of pots for chaos divisor (one per trigger out)
  * 1 CV in for control of chaos
  * 6 analog inputs remaining
  * SIG: 27 (A13)
  * 28: S0
  * 29: S1
  * 30: S2
  * 31: S3
* 5: Jumper pin for gate/trigger...rather than a jumper, shall we use a switch? and maybe a trim pot on the back?
* 6: Juper pin for using RTC or not
* 

*/

#define AUDIO_MEMORY        24

#define GATE_TRIG_JUMPER    5
#define RTC_JUMPER          6

#define CHAOS_CV_PIN        A12
#define SCALE_CV_PIN        A13

#define ENCODER_1           2
#define ENCODER_2           3
#define ENCODER_BUTTON      4

#define CS_PIN              10
#define DIN_PIN             11
#define CLK_PIN             13

#define TRIG_GATE_SW        5
#define TRIG_STATE          1
#define GATE_STATE          0

#define SET_BUTTON          8
#define DIVISOR_BUTTON      9

#define MAX_CHAOS_PINS      3
#define CHAOS_PINS_RESOLUTION 4096
#define MAX_CHAOS_VALUE     300.0

// How many cycles are we working with?
#define MAX_CYCLES  8
#define MAX_VOICES  8

#define DEFAULT_WAVEFORM WAVEFORM_SAWTOOTH

// trigger/gate length in ms
#define DEFAULT_TRIGGER_LENGTH  10
#define DEFAULT_GATE_LENGTH     100

// C4
#define DEFAULT_FUNAMDENTAL_FREQUENCY 261.63

#define NEVER_RUN_STATE       0x00
#define POST_FIRST_RUN_STATE  0xFF

// Default latitude and longitude
// Given as floats, so decimal notation
#define DEFAULT_LAT   0.00
#define DEFAULT_LON   0.00

// Output trigger/gate channels and their correspondences in MIDI
#define ENABLE_MIDI         0
#define DEFAULT_MIDI_CHAN   5
#define DEFAULT_MIDI_VEL    127
#define TRIG1_MIDI          0
#define TRIG2_MIDI          1
#define TRIG3_MIDI          2
#define TRIG4_MIDI          3
#define TRIG5_MIDI          4
#define TRIG6_MIDI          5
#define TRIG7_MIDI          6
#define TRIG8_MIDI          7

#define KNOB_MIN            0
#define KNOB_MAX            80

#define MAX_DISPLAY_ROWS    8

#define MAX_COMMENT_LEN     256

extern int midiNotes[MAX_CYCLES];
extern int8_t midiFlags[MAX_CYCLES];

//using Transport = MIDI_NAMESPACE::SerialMIDI<HardwareSerial>;
//HardwareSerial mySerial = HardwareSerial(Serial1);
//Transport serialMIDI(mySerial);
//typedef MIDI_NAMESPACE::MidiInterface<Transport> MyMIDI;
//extern MyMIDI TestMIDI;



// Data structure for holding information about each cycle
struct Cycle {
  uint64_t period;
  float scale;
  uint64_t periodCorrected;
  uint32_t offset;
  char comment[MAX_COMMENT_LEN];
};

struct CyclesSet {
  //Cycle *(cyclesSet[MAX_CYCLES]);
  //int testing[8];
  Cycle cyclesSet[MAX_CYCLES];
};


// Initialize our cycles array
extern Cycle cycles[MAX_CYCLES];

extern Cycle set001[MAX_CYCLES];
extern Cycle set002[MAX_CYCLES];
extern Cycle set003[MAX_CYCLES];
extern Cycle set004[MAX_CYCLES];
extern Cycle set005[MAX_CYCLES];
extern Cycle set006[MAX_CYCLES];
extern Cycle set007[MAX_CYCLES];
extern Cycle set008[MAX_CYCLES];

extern CyclesSet cyclesSet001;
extern CyclesSet cyclesSet002;
extern CyclesSet cyclesSet003;
extern CyclesSet cyclesSet004;
extern CyclesSet cyclesSet005;
extern CyclesSet cyclesSet006;
extern CyclesSet cyclesSet007;
extern CyclesSet cyclesSet008;
extern CyclesSet cyclesSets[MAX_CYCLES];

extern float cycleScales[MAX_CYCLES];

extern ResponsiveAnalogRead chaosCV;
extern ResponsiveAnalogRead scaleCV;

// Envelope structure, along with length of NoteOn
typedef struct {
  int attack;
  int decay;
  float sustain;
  int release;
  int duration;
} Env;

extern Env envelopeValues[MAX_CYCLES];

extern int noteDurations[MAX_CYCLES];
extern int noteDurationsCounter[MAX_CYCLES];

extern float defaultNoteRatios[MAX_CYCLES];
extern float noteRatios[MAX_CYCLES];

extern float fundamentalFrequency;

extern int triggerDurations[MAX_CYCLES];

extern int chaosPins[MAX_CHAOS_PINS];
extern int triggerPins[MAX_CYCLES];
extern int chaosIntensityPins[MAX_CYCLES];
extern u_int16_t chaosPinsValues[MAX_CHAOS_PINS];
extern ResponsiveAnalogRead chaosPinX;
extern ResponsiveAnalogRead chaosPinY;
extern ResponsiveAnalogRead chaosPinZ;
extern Smoothed <float> *chaosPinsAvgs[MAX_CHAOS_PINS];
extern Smoothed <float> chaosPinXAvg; 
extern Smoothed <float> chaosPinYAvg;
extern Smoothed <float> chaosPinZAvg;
extern ResponsiveAnalogRead *chaosPinsReads[MAX_CHAOS_PINS];
extern ResponsiveAnalogRead chaosIntensityPot1;
extern ResponsiveAnalogRead chaosIntensityPot2;
extern ResponsiveAnalogRead chaosIntensityPot3;
extern ResponsiveAnalogRead chaosIntensityPot4;
extern ResponsiveAnalogRead chaosIntensityPot5;
extern ResponsiveAnalogRead chaosIntensityPot6;
extern ResponsiveAnalogRead chaosIntensityPot7;
extern ResponsiveAnalogRead chaosIntensityPot8;
extern ResponsiveAnalogRead *chaosIntensitiesPots[MAX_CYCLES];
extern float chaosIntensities[MAX_CYCLES];
extern float defaultVoiceScalesSet[MAX_VOICES + 1];
extern float voiceScalesSet[MAX_VOICES + 1];
extern float allVoiceScales[MAX_VOICES][MAX_CYCLES];

extern uint16_t firstRun;

extern bool trigGate;

extern float latValue;
extern float lonValue;

//extern char muxValue;
extern uint16_t chaosCVValue;
extern uint16_t scaleCVValue;


//extern LedControl display;
extern LedControl_HW_SPI display;
//#define HARDWARE_TYPE MD_MAX72XX::PAROLA_HW
//#define HARDWARE_TYPE MD_MAX72XX::GENERIC_HW
#define MAX_DEVICES 1
//extern MD_Parola display;
const uint8_t F_SIGIL = 1;
const uint8_t W_SIGIL = 8;
extern const uint64_t sigil_image000;
extern const uint64_t sigil_image001;
extern const uint64_t sigil_image002;
extern const uint64_t sigil_image003;
extern const uint64_t sigil_image004;
extern const uint64_t sigil_image005;
extern const uint64_t sigil_image006;
extern const uint64_t sigil_image007;
extern const uint64_t sigilList[MAX_CYCLES];
extern const uint64_t voiceDivisorInterface;
extern const char barValues[MAX_DISPLAY_ROWS + 1];
extern uint64_t scaleImage;
extern const uint8_t sigil[F_SIGIL * W_SIGIL];
extern const uint8_t sigil_test[8];
//extern const uint64_t sigil;

extern const uint64_t bootImages[MAX_DISPLAY_ROWS + 1];

extern Bounce setButton;
extern uint8_t setIndex;

extern bool trigGateState;

extern const String cyclesSetsFilenames[MAX_CYCLES];

void initializeAllVoiceScales();
void initializeChaosPinsAvgs();

// Taken from here: <https://stackoverflow.com/questions/4548004/how-to-correctly-and-standardly-compare-floats>
bool areEqualRel(float a, float b, float epsilon = 0.0000001); 
#endif
